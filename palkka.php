<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    
    <?php    
        $eurot = filter_input(INPUT_POST, 'eurot', FILTER_SANITIZE_NUMBER_INT);
        $ep = filter_input(INPUT_POST, 'pros', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tem = filter_input(INPUT_POST, 'pros1', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tvm = filter_input(INPUT_POST, 'pros2', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        $ltvm = $eurot * ($tvm / 100);
        $ltem = $eurot * ($tem / 100); 
        $lep = $eurot * ($ep / 100);
        $np = $eurot - ($lep + $ltem + $ltvm);
        
        
        printf("<p>Ennakkopidätys: %.2f€</p>", $lep);
        printf("<p>Työeläkemaksu: %.2f€</p>", $ltem);
        printf("<p>Työttömyysvakuutusmaksu: %.2f€</p>", $ltvm);
        printf("<p>Nettopalkka on %.2f€</p>", $np);
    ?>

    <a href="index.html">laske uudelleen</a>
</body>
</html>